package com.example.yeon.final_project;

import android.app.ListActivity;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class MylistActivity extends ListActivity {
    String name = null;
    String address = null;
    String menu = null;
    String reuse = null;
//    public ImageView imageIcon;
//imageIcon = (ImageView)findViewById(R.id.image_myList);
    //imageIcon.setImageResource(R.drawable.gift);
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mylist);

//        ListView listview;
//        ListAdapter adapter;
//
//        // Adapter 생성
//        adapter = new ListAdapter();
//
//        // 리스트뷰 참조 및 Adapter달기
//        listview = (ListView) findViewById(R.id.listview);
//        listview.setAdapter(adapter);
//

    }

    @Override
    protected void onResume() {
        super.onResume();
        loadNewDB();
    }
    public void loadNewDB(){
        db = openOrCreateDatabase("test.db", SQLiteDatabase.CREATE_IF_NECESSARY, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS MyList " + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, menu TEXT, reuse TEXT, address TEXT);");

        Cursor c = db.rawQuery("SELECT * FROM MyList;", null);
        startManagingCursor(c);
        SimpleCursorAdapter adapt = new SimpleCursorAdapter(this, R.layout.my_listitem, c, new String[] {"name", "menu", "reuse"}, new int[] {R.id.text_name, R.id.text_menu, R.id.text_reuse}, 0);

        setListAdapter(adapt);


    }

    @Override
    protected void onPause() {
        super.onPause();

        if(db != null){
            db.close();
        }

    }

    public void plusDialog(View v) {//추가 기능 구현

        LinearLayout allim = (LinearLayout) View.inflate(MylistActivity.this, R.layout.plus_dialog, null);

        AlertDialog.Builder aDialog = new AlertDialog.Builder(this);
        aDialog.setTitle("나의 재활용 아이템 추가");
        aDialog.setView(allim);

        final EditText nameEdit = (EditText) allim.findViewById(R.id.edit_name);
        final EditText addressEdit = (EditText) allim.findViewById(R.id.edit_address);

        final Spinner spin_menu=(Spinner)allim.findViewById(R.id.spinner_menu);
        final Spinner spin_reuse=(Spinner)allim.findViewById(R.id.spinner_reuse);


        aDialog.setPositiveButton("추가 설정", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                name = nameEdit.getText().toString();
                address = addressEdit.getText().toString();
                menu = spin_menu.getSelectedItem().toString();
                reuse = spin_reuse.getSelectedItem().toString();

                db = openOrCreateDatabase("test.db", SQLiteDatabase.CREATE_IF_NECESSARY, null);
                db.execSQL("INSERT INTO MyList (name,menu,reuse,address) VALUES ('" + name + "','" + menu + "','" + reuse + "','"+address+"');");
                db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse) VALUES ('"+menu+"','"+name+"',"
                        + "'"+address+"',"+0+","+0+",'gift','"+reuse+"');");

                Cursor c = db.rawQuery("SELECT * FROM MyList;", null);
                startManagingCursor(c);
                SimpleCursorAdapter adapt = new SimpleCursorAdapter(MylistActivity.this, R.layout.my_listitem, c, new String[] {"name", "menu", "reuse"}, new int[] {R.id.text_name, R.id.text_menu, R.id.text_reuse}, 0);
                //imageIcon = (ImageView)findViewById(R.id.image_myList);
                //imageIcon.setImageResource(R.drawable.gift);

                setListAdapter(adapt);

//                if(db != null){
//                    db.close();
//                }

                 Toast.makeText(MylistActivity.this, "추가되었습니다.", Toast.LENGTH_SHORT).show();
            }
        });

        aDialog.setNegativeButton("취소", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MylistActivity.this, "취소 되었습니다.", Toast.LENGTH_SHORT).show();
            }
        });
        aDialog.setCancelable(false);
        aDialog.show();
    }

}
