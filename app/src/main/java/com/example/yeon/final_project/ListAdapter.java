package com.example.yeon.final_project;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by User on 2017-11-29.
 */

public class ListAdapter extends SimpleCursorAdapter {

    private Context imageContext;

 public ListAdapter(Context context, int layout, Cursor c, String[] from, int[] to){
     super(context, layout, c, from, to);
     this.imageContext = context;
 }


    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        final int pos=position;
        final Context context=parent.getContext();

        ViewHolder vh;

        if(convertView==null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listitem, parent, false);

            vh = new ViewHolder();

            vh.productName = (TextView) convertView.findViewById(R.id.product_name);
            vh.bestNum = (TextView) convertView.findViewById(R.id.best_number);
            vh.material = (TextView) convertView.findViewById(R.id.material);
            vh.imageIcon=(ImageView) convertView.findViewById(R.id.coupon_icon);
            vh.starButton=(ImageButton) convertView.findViewById(R.id.star_button);
            vh.thumbButton=(ImageButton) convertView.findViewById(R.id.thumb_button);
            vh.layout=(RelativeLayout) convertView.findViewById(R.id.list);
            vh.idText=(TextView) convertView.findViewById(R.id.idText);
            vh.category=(TextView)convertView.findViewById(R.id.cat);

            convertView.setTag(vh);
        }
        else
            vh=(ViewHolder) convertView.getTag();

        Cursor c = (Cursor)this.getItem(position);

        String name=c.getString(2);
        int num=c.getInt(5);
        String material=c.getString(7);
        String image_name = c.getString(6);
        String category = c.getString(1);
        int resId = imageContext.getResources().getIdentifier(image_name, "drawable",
                "com.example.yeon.final_project");
        final String url = c.getString(3);
        final TextView numText=(TextView) convertView.findViewById(R.id.best_number);
        final TextView idText=(TextView) convertView.findViewById(R.id.idText);

        int id=c.getInt(0);

        vh.productName.setText(name);
        vh.bestNum.setText(Integer.toString(num));
        vh.material.setText(material);
        vh.imageIcon.setImageResource(resId);
        vh.idText.setText(Integer.toString(id));
        vh.category.setText(category);

        if(c.getInt(4)==1)
        {
            vh.starButton.setImageResource(R.drawable.star_on);
            vh.starButton.setSelected(true);
        }
        else
        {
            vh.starButton.setImageResource(R.drawable.star_off);
            vh.starButton.setSelected(false);
        }


        if(c.getInt(8)==1)
        {
            vh.thumbButton.setImageResource(R.drawable.thumb_on);
            vh.thumbButton.setSelected(true);
        }
        else{
            vh.thumbButton.setImageResource(R.drawable.thumb_off);
            vh.thumbButton.setSelected(false);
        }


        vh.layout.setOnClickListener(new View.OnClickListener() {//웹 사이트 연결
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });


        vh.starButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                String id=idText.getText().toString();

                ImageButton starButton=(ImageButton) view.findViewById(R.id.star_button);
                SQLiteDatabase db=context.openOrCreateDatabase("test.db", SQLiteDatabase.CREATE_IF_NECESSARY, null);
                String sql = "update people set star =";

                if(starButton.isSelected())
                {
                    starButton.setImageResource(R.drawable.star_off);
                    starButton.setSelected(false);


                    sql+="0 where _id="+id+";";
                    db.execSQL(sql);

                    db.close();

                   Toast.makeText(context, "즐겨찾기가 해제되었습니다.", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    starButton.setImageResource(R.drawable.star_on);
                    starButton.setSelected(true);


                    sql+="1 where _id="+id+";";
                    db.execSQL(sql);
                    db.close();

                    Toast.makeText(context, "즐겨찾기가 설정되었습니다.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        vh.thumbButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                String id=idText.getText().toString();
                ImageButton thumbButton=(ImageButton) view.findViewById(R.id.thumb_button);
                int num=Integer.parseInt(numText.getText().toString());

                SQLiteDatabase db=context.openOrCreateDatabase("test.db", SQLiteDatabase.CREATE_IF_NECESSARY, null);
                String sql = "update people set best =";
                String best_sql="update people set like =";

                if(thumbButton.isSelected())
                {
                    thumbButton.setImageResource(R.drawable.thumb_off);
                    thumbButton.setSelected(false);
                    sql+=Integer.toString(num-1)+" where _id="+id+";";
                    best_sql+="0 where _id="+id+";";

                    db.execSQL(sql);
                    db.execSQL(best_sql);
                    db.close();

                    numText.setText(Integer.toString(num-1));
                }
                else
                {
                    thumbButton.setImageResource(R.drawable.thumb_on);
                    thumbButton.setSelected(true);
                    sql+=Integer.toString(num+1)+" where _id="+id+";";
                    best_sql+="1 where _id="+id+";";

                    db.execSQL(sql);
                    db.execSQL(best_sql);
                    db.close();

                    numText.setText(Integer.toString(num+1));
                }
            }
        });

        return convertView;
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent){
        LayoutInflater inflater=LayoutInflater.from(context);

        View v=inflater.inflate(R.layout.listitem, parent, false);

        return  v;
    }


    public class ViewHolder{
        public TextView bestNum;
        public TextView productName;
        public TextView material;
        public ImageView imageIcon;
        public ImageButton starButton;
        public ImageButton thumbButton;
        public RelativeLayout layout;
        public TextView idText;
        public TextView category;
    }
}
