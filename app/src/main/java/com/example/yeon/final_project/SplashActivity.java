package com.example.yeon.final_project;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ImageView imageView = (ImageView)findViewById(R.id.imageView4);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate);
        imageView.setAnimation(animation);

        ImageView imageView0 = (ImageView)findViewById(R.id.imageView0);
        Animation animation2 = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.dropdown);
        imageView0.setAnimation(animation2);

        Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                finish();
                overridePendingTransition(R.anim.fadein,R.anim.fadeout);
            }
        };
        handler.sendEmptyMessageDelayed(0,2000);
    }

    public void onBackPressed(){}
}