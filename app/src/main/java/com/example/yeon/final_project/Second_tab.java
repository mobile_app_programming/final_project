package com.example.yeon.final_project;

import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class Second_tab extends ListActivity {
    SQLiteDatabase db;
    final int ALL =0;
    final int INTERIOR=1;
    final int PLAY =2;
    final int STORAGE_GOODS=3;
    final int STUFF_FASHION=4;
    final int KITCHEN_UTENSILS=5;
    String type;

    int i =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_tab);
        Spinner spin=(Spinner)findViewById(R.id.spinner);

        ArrayAdapter<CharSequence> adapName=ArrayAdapter.createFromResource(
                this, R.array.name, android.R.layout.simple_spinner_item);
        adapName.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(adapName);

        spin.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener(){
                    public void onItemSelected(AdapterView<?> parent, View v, int pos, long id){
                        switch(pos)
                        {
                            case ALL:
                                type="all";
                                break;
                            case INTERIOR:
                                type="인테리어";
                                break;
                            case PLAY:
                                type ="놀이";
                                break;
                            case STORAGE_GOODS:
                                type="수납";
                                break;
                            case STUFF_FASHION:
                                type="잡화,패션";
                                break;
                            case KITCHEN_UTENSILS:
                                type="주방용품";
                                break;
                            }
                        loadDB();
                    }
                    public void onNothingSelected(AdapterView<?> parent){
                    }
                }
        );
    }

    public void loadDB(){
        String sql;
        db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );

        if(type.equals("all")){
            sql = "SELECT * FROM people ORDER BY best DESC";//전체이면 모두 검색
        }
        else {
            sql = "SELECT * FROM people WHERE menu = '" + type + "' ORDER BY best DESC;";//해당 메뉴에 해당하는 데이터 검색
        }

        Cursor c = db.rawQuery(sql, null);
        startManagingCursor(c);
        ListAdapter adapter = new ListAdapter(
                this,
                R.layout.listitem,
                c,
                new String[] {"name","reuse","best","menu"},
                new int[] {R.id.product_name, R.id.material, R.id.best_number,R.id.cat}
        );

        this.setListAdapter(adapter);

    }

    @Override
    protected void onPause() {
        super.onPause();

        db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );

        if (db != null) {
            db.close();
        }
    }
}
