package com.example.yeon.final_project;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

public class MaterialListActivity extends AppCompatActivity {
    String condition = null;
    SQLiteDatabase db;
    Cursor c;
    ListAdapter adapter;
    String sql;
    ListView listView;
    String title = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_list);

        condition = getIntent().getExtras().getString("condition");

        switch (condition){
            case "paper":
                title = "종이류";
                break;
            case "plastic":
                title = "플라스틱";
                break;
            case "bottle":
                title = "유리병";
                break;
            case "milk_pack":
                title = "우유갑";
                break;
            case "병뚜껑":
                title = "병뚜껑";
                break;
            case "can":
                title = "캔";
                break;
            case "ice cream stick":
                title = "아이스크림 막대";
                break;
            case "clothes hanger":
                title = "옷걸이";
                break;
            case "button":
                title = "단추";
                break;
            case "egg":
                title = "계란판";
                break;
            case "styrofoam":
                title = "스티로폼";
                break;
            case "toielt tissue":
                title = "휴지심";
                break;
        }
        getSupportActionBar().setTitle(title);
        setContentView(R.layout.activity_material_list);

        loadDB();

    }

    public void loadDB() {

        db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );


        listView = (ListView) findViewById(R.id.item_list);

        sql = "SELECT * FROM people WHERE reuse = '" + condition + "'ORDER BY best DESC;";
        c = db.rawQuery(sql, null);
        startManagingCursor(c);
        adapter = new ListAdapter(
                listView.getContext(),
                R.layout.listitem,
                c,
                new String[]{"name", "reuse", "best","menu"},
                new int[]{R.id.product_name, R.id.material, R.id.best_number,R.id.cat}
        );

        listView.setAdapter(adapter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );

        if (db != null) {
            db.close();
        }
    }


}

