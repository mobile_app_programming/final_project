package com.example.yeon.final_project;

import android.app.ActivityGroup;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends ActivityGroup{

    String sql = null;
    private TabHost tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = new Intent(this,SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        
        tabHost = (TabHost) findViewById(R.id.tabHost);

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                for(int i=0; i<tabHost.getTabWidget().getChildCount(); i++){
                    TextView tv=(TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
                    tv.setTextColor(Color.parseColor("gray"));

                    tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#5fc1d497"));

                }

                TextView tp=(TextView) tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).findViewById(android.R.id.title);
                tp.setTextColor(Color.parseColor("white"));

                tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#7bb17d"));

            }
        });


        tabHost.setup(getLocalActivityManager());//ActivityGroup 을 extends 해야한다.

        tabHost.addTab(tabHost.newTabSpec("tab1")
                .setIndicator("재료선택")//image 넣고 싶으면 .setIndicator("",getResources().getDrawable(R.drawable.이미지명))
                .setContent(new Intent(this, ActivityHome.class)));

        tabHost.addTab(tabHost.newTabSpec("tab2")
                .setIndicator("품목선택")
                .setContent(new Intent(this, Second_tab.class)));

        tabHost.addTab(tabHost.newTabSpec("tab3")
                .setIndicator("즐겨찾기")
                .setContent(new Intent(this, Third_tab.class)));

        tabHost.addTab(tabHost.newTabSpec("tab4")
                .setIndicator("추천영상")
                .setContent(new Intent(this, Fourth_tab.class)));



        loadDB();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_mylist){
            startActivity(new Intent(this,MylistActivity.class));
            return true;
        }
        else if(id==R.id.action_share){
            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setType("text/plain");

            String text = "원하는 텍스트를 입력하세요";
            intent.putExtra(Intent.EXTRA_TEXT, text);

            Intent chooser = Intent.createChooser(intent, "친구에게 공유하기");
            startActivity(chooser);

        }
        return super.onOptionsItemSelected(item);
    }
    public String RandomFunc(){
        Random random = new Random();
       return Integer.toString(random.nextInt(100));
    }

    public String RNumP()
    {
        Random random=new Random();

        if(random.nextInt(100)<=23)
            return "1";
        else
            return "0";
    }

    public void loadDB() {
        deleteDatabase("test.db");
        SQLiteDatabase db = openOrCreateDatabase("test.db", SQLiteDatabase.CREATE_IF_NECESSARY, null);
        // 우유갑
        db.execSQL("CREATE TABLE IF NOT EXISTS people " + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, menu TEXT, name TEXT, link TEXT, star INTEGER, best INTEGER, image TEXT, reuse TEXT, like INTEGER);");
        db.execSQL("CREATE TABLE IF NOT EXISTS MyList " + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, menu TEXT, reuse TEXT, address TEXT);");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','여성용품보관함',"
                + "'http://blog.naver.com/goglass/70158532846',"+RNumP()+","+RandomFunc()+",'a1','milk_pack', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','티백보관함',"
                + "'http://blog.naver.com/goglass/70163067043',"+RNumP()+","+RandomFunc()+",'a2','milk_pack', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('인테리어','장식품',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=10571&memberNo=1875',"+RNumP()+","+RandomFunc()+",'a3','milk_pack', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','선물포장',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=10571&memberNo=1875',"+RNumP()+","+RandomFunc()+",'a4','milk_pack', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('놀이','한글교구',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=1918655&memberNo=592664',"+RNumP()+","+RandomFunc()+",'a5','milk_pack', "+RNumP()+");");

        //플라스틱
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('주방용품','양념통',"
                + "'http://naver.me/FcR30CA8',1,"+RandomFunc()+",'b1','plastic', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','우산꽂이',"
                + "'http://blog.naver.com/minkang8191/220443709388',"+RNumP()+","+RandomFunc()+",'b2','plastic', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','연필꽂이',"
                + "'http://blog.naver.com/dumin34/221076953422',"+RNumP()+","+RandomFunc()+",'b3','plastic', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('놀이','이글루',"
                + "'http://blog.naver.com/goglass/220742428630',"+RNumP()+","+RandomFunc()+",'b4','plastic', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','벽시계',"
                + "'http://blog.naver.com/goglass/70177974640',"+RNumP()+","+RandomFunc()+",'b5','plastic', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('놀이','미니바다',"
                + "'http://naver.me/GM6g7whP',"+RNumP()+","+RandomFunc()+",'b6','plastic', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','핀쿠션',"
                + "'http://lview.m.ezday.co.kr/app/view_board.html?q_id_info=320&q_sq_board=3343464',"+RNumP()+","+RandomFunc()+",'b7','plastic', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('인테리어','인테리어소품',"
                + "'http://blog.naver.com/fromrei8/220818447847',"+RNumP()+","+RandomFunc()+",'b8','plastic', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','액세서리보관함',"
                + "'http://blog.daum.net/misiya01/4194630',"+RNumP()+","+RandomFunc()+",'b9','plastic', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('놀이','줄넘기',"
                + "'http://blog.naver.com/pjmysm/220048280193',"+RNumP()+","+RandomFunc()+",'b10','plastic', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','비닐함 정리',"
                + "'https://www.freehugatopy.com/board/?document_srl=101204',"+RNumP()+","+RandomFunc()+",'b11','plastic', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','칫솔꽂이',"
                + "'http://healthlife16.tistory.com/914',"+RNumP()+","+RandomFunc()+",'b12','plastic', "+RNumP()+");");


        //유리병
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('인테리어','인테리어소품',"
                + "'https://ohou.se/advices/114',"+RNumP()+","+RandomFunc()+",'c1','bottle', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','수경화분',"
                + "'http://www.pulmuonelohas.com/archives/6105',"+RNumP()+","+RandomFunc()+",'c2','bottle', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('주방용품','주방수납',"
                + "'https://www.ggumim.co.kr/star/view/303',"+RNumP()+","+RandomFunc()+",'c3','bottle', "+RNumP()+");");

        //옷걸이
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','벨트걸이',"
                + "'http://blog.naver.com/goglass/70141933334',"+RNumP()+","+RandomFunc()+",'d1','clothes hanger', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('인테리어','리스',"
                + "'http://blog.naver.com/bigbigtoe/220571770141',"+RNumP()+","+RandomFunc()+",'d2','clothes hanger', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('주방용품','행주걸이',"
                + "'https://blog.naver.com/PostView.nhn?blogId=blue_apple&logNo=40113252138&proxyReferer=https:%2F%2Fwww.google.co.kr%2F',"+RNumP()+","+RandomFunc()+",'d3','clothes hanger', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('인테리어','드림캐쳐',"
                + "'http://www.ezday.co.kr/bbs/view_board.html?q_sq_board=7606330',"+RNumP()+","+RandomFunc()+",'d4','clothes hanger', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','책받침대',"
                + "'http://beakpro.tistory.com/237',"+RNumP()+","+RandomFunc()+",'d5','clothes hanger', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('주방용품','수세미걸이',"
                + "'http://bomb-two.tistory.com/entry/%EC%98%B7%EA%B1%B8%EC%9D%B4%ED%99%9C%EC%9A%A9-%EC%88%98%EC%84%B8%EB%AF%B8%EA%B1%B8%EC%9D%B4',"+RNumP()+","+RandomFunc()+",'d6','clothes hanger', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','거치대',"
                + "'http://blog.naver.com/pjmysm/220007356130',"+RNumP()+","+RandomFunc()+",'d7','clothes hanger', "+RNumP()+");");

        //단추
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','책갈피',"
                + "'http://blog.naver.com/g-gle/220839885205',"+RNumP()+","+RandomFunc()+",'e1','button', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','액세서리',"
                + "'http://blog.naver.com/soable17/140187382224',"+RNumP()+","+RandomFunc()+",'e2','button', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','액세서리',"
                + "'http://blog.naver.com/ljw9939/80051025036',"+RNumP()+","+RandomFunc()+",'e3','button', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','액세서리',"
                + "'http://blog.naver.com/uk7575/20196371634',"+RNumP()+","+RandomFunc()+",'e4','button', "+RNumP()+");");

        //병뚜껑
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','자석',"
                + "'http://blog.naver.com/rhkdsu01/220263623684',"+RNumP()+","+RandomFunc()+",'f1','병뚜껑', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','냄비받침',"
                + "'http://blog.naver.com/oranganna/220210327155',"+RNumP()+","+RandomFunc()+",'f2','병뚜껑', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','액세서리',"
                + "'http://blog.naver.com/tamitam/220835749518',"+RNumP()+","+RandomFunc()+",'f3','병뚜껑', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','캔들받침',"
                + "'http://blog.naver.com/pjmysm/220119351310',"+RNumP()+","+RandomFunc()+",'f4','병뚜껑', "+RNumP()+");");

        //계란판
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('인테리어','리스',"
                + "'http://blog.naver.com/7salsa/220442633017',"+RNumP()+","+RandomFunc()+",'g1','egg', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('인테리어','메모꽂이',"
                + "'http://blog.naver.com/sarangdama/168697052',"+RNumP()+","+RandomFunc()+",'g2','egg', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','달력',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=1709771&memberNo=592664',"+RNumP()+","+RandomFunc()+",'g3','egg', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('인테리어','미니트리',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=14439&memberNo=1875',"+RNumP()+","+RandomFunc()+",'g4','egg', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('인테리어','꽃모형',"
                + "'http://blog.naver.com/kelly5135/220290050355',"+RNumP()+","+RandomFunc()+",'g5','egg', "+RNumP()+");");

        //아이스크림 막대
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('인테리어','액자',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=3354354&memberNo=592664',"+RNumP()+","+RandomFunc()+",'h1','ice cream stick', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','책갈피',"
                + "'http://blog.naver.com/g-gle/220839885205',"+RNumP()+","+RandomFunc()+",'h2','ice cream stick', "+RNumP()+");");

        //스티로폼
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('인테리어','메모보드',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=3813359&memberNo=830',"+RNumP()+","+RandomFunc()+",'i1','styrofoam', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('인테리어','리스',"
                + "'http://blog.naver.com/sky20001224/40123326197',"+RNumP()+","+RandomFunc()+",'i2','styrofoam', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('인테리어','소파',"
                + "'http://blog.naver.com/jonggulbak/150165398110',"+RNumP()+","+RandomFunc()+",'i3','styrofoam', "+RNumP()+");");

        //캔
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','벽걸이수납',"
                + "'http://blog.naver.com/goglass/70173321256',"+RNumP()+","+RandomFunc()+",'j1','can', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','화분',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=4312070&memberNo=621662',"+RNumP()+","+RandomFunc()+",'j2','can', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','연필꽂이',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=348826&memberNo=1746',"+RNumP()+","+RandomFunc()+",'j3','can', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','소품보관함',"
                + "'http://blog.naver.com/happyhannamu/220615657431',"+RNumP()+","+RandomFunc()+",'j4','can', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','지갑',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=327663&memberNo=1376362',"+RNumP()+","+RandomFunc()+",'j5','can', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('인테리어','캔라이트',"
                + "'http://blog.naver.com/deoninterior/221045954160',"+RNumP()+","+RandomFunc()+",'j6','can', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('주방용품','수저통',"
                + "'http://blog.naver.com/fkausl9574/220936662230',"+RNumP()+","+RandomFunc()+",'j7','can', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','액세서리함',"
                + "'http://m.post.naver.com/viewer/postView.nhn?memberNo=783&volumeNo=6819497&vType=VERTICAL',"+RNumP()+","+RandomFunc()+",'j8','can', "+RNumP()+");");

        //휴지심
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('놀이','휴지심책',"
                + "'http://naver.me/FFb2hR0a',"+RNumP()+","+RandomFunc()+",'k1','toielt tissue', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','선물포장',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=322753&memberNo=1376362',"+RNumP()+","+RandomFunc()+",'k2','toielt tissue', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','선물포장',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=322766&memberNo=1376362',"+RNumP()+","+RandomFunc()+",'k3','toielt tissue', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','연필꽂이',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=1627669&memberNo=230561',"+RNumP()+","+RandomFunc()+",'k4','toielt tissue', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','전선정리',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=326520&memberNo=659',"+RNumP()+","+RandomFunc()+",'k5','toielt tissue', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','화분',"
                + "'http://blog.naver.com/ljlj0802/220312621554',"+RNumP()+","+RandomFunc()+",'k6','toielt tissue', "+RNumP()+");");

        //종이류
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('놀이','열기구장난감',"
                + "'http://blog.hanwhadays.com/3908',"+RNumP()+","+RandomFunc()+",'l1','paper', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','수납장',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=9773916&memberNo=36512651',"+RNumP()+","+RandomFunc()+",'l2','paper', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','영수증보관함',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=1366705&memberNo=574&vType=VERTICAL',"+RNumP()+","+RandomFunc()+",'l3','paper', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','수납용품',"
                + "'http://blog.naver.com/krishnaa/120092517607',"+RNumP()+","+RandomFunc()+",'l4','paper', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','벽시계',"
                + "'http://post.naver.com/viewer/postView.nhn?volumeNo=3175002&memberNo=659',"+RNumP()+","+RandomFunc()+",'l5','paper', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','여권케이스',"
                + "'http://blog.naver.com/xzwe/221098555267',"+RNumP()+","+RandomFunc()+",'l6','paper', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','수납상자',"
                + "'http://blog.naver.com/dearwoony/220488226115',"+RNumP()+","+RandomFunc()+",'l7','paper', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','선물포장',"
                + "'http://blog.naver.com/hohuk212/220262425964',"+RNumP()+","+RandomFunc()+",'l8','paper', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','비닐봉투정리',"
                + "'http://blog.naver.com/oufz432xeud/220915813816',"+RNumP()+","+RandomFunc()+",'l9','paper', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','선물포장',"
                + "'http://blog.naver.com/bigbigtoe/220533686937',"+RNumP()+","+RandomFunc()+",'l10','paper', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','정리용품',"
                + "'http://blog.naver.com/pascucci1883/220636102310',"+RNumP()+","+RandomFunc()+",'l11','paper', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','캔들케이스',"
                + "'http://blog.naver.com/alansul78/220887512359',"+RNumP()+","+RandomFunc()+",'l12','paper', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('잡화,패션','저금통',"
                + "'http://blog.naver.com/junga78/221042164706',"+RNumP()+","+RandomFunc()+",'l13','paper', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('인테리어','무드등',"
                + "'http://blog.naver.com/pjmysm/220716913215',"+RNumP()+","+RandomFunc()+",'l14','paper', "+RNumP()+");");
        db.execSQL("INSERT INTO people (menu,name,link,star,best,image,reuse,like) VALUES ('수납','파일박스',"
                + "'http://blog.naver.com/pjmysm/220715388230',"+RNumP()+","+RandomFunc()+",'l15','paper', "+RNumP()+");");

    }


    @Override
    protected void onDestroy(){
        super.onDestroy();

        SQLiteDatabase db = openOrCreateDatabase("test.db", SQLiteDatabase.CREATE_IF_NECESSARY, null);

        if (db != null) {
            db.close();
        }
    }
}

