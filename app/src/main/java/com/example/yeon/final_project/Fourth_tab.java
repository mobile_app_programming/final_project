package com.example.yeon.final_project;

import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import static android.R.attr.id;

public class Fourth_tab extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("추천 영상");
        setContentView(R.layout.activity_fourth_tab);

    }


    public void onClickButton(View view) {
        switch (view.getId()){
            case R.id.movie1:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=WSJUOzgTbbM&list=PLZhmbapHXs1_dcNZuMzUkhBxvEHIRg6Dw")));
                break;
            case R.id.movie2:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=Lt7OR_5xNh4")));
                break;
            case R.id.movie3:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=ftLjAdg6bos")));
                break;
        }
    }

    public void onClickShareButton(View view) {
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        String text;
        switch (view.getId()) {
            case R.id.button1:
               text = "https://www.youtube.com/watch?v=WSJUOzgTbbM&list=PLZhmbapHXs1_dcNZuMzUkhBxvEHIRg6Dw";
                intent.putExtra(Intent.EXTRA_TEXT, text);
                break;
            case R.id.button2:
                text = "https://www.youtube.com/watch?v=Lt7OR_5xNh4";
                intent.putExtra(Intent.EXTRA_TEXT, text);
                break;
            case R.id.button3:
                text = "https://www.youtube.com/watch?v=ftLjAdg6bos";
                intent.putExtra(Intent.EXTRA_TEXT, text);
                break;
        }

        Intent chooser = Intent.createChooser(intent, "친구에게 공유하기");
        startActivity(chooser);

    }
}
