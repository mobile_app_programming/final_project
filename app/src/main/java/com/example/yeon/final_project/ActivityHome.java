package com.example.yeon.final_project;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ActivityHome extends AppCompatActivity {

    final int PAPER =0;
    final int PLASTIC=1;
    final int BOTTLE=2;
    final int MILK_PACK=3;
    final int COCA_COLA=4;
    final int CAN=5;
    final int ICE_CREAM_STICK=6;
    final int CLOTHES_HANGER=7;
    final int BUTTON=8;
    final int EGG=9;
    final int STYROFOAM=10;
    final int TOILET_TISSUE=11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("재료선택");
        setContentView(R.layout.activity_home);

        GridView gridView=(GridView) findViewById(R.id.gridView);
        gridView.setAdapter(new GridViewAdapter(this));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            String type;
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                switch(position)
                {
                    case PAPER:
                        type="paper";
                        break;
                    case PLASTIC:
                        type="plastic";
                        break;
                    case BOTTLE:
                        type="bottle";
                        break;
                    case MILK_PACK:
                        type="milk_pack";
                        break;
                    case COCA_COLA:
                        type="병뚜껑";
                        break;
                    case CAN:
                        type="can";
                        break;
                    case ICE_CREAM_STICK:
                        type="ice cream stick";
                        break;
                    case CLOTHES_HANGER:
                        type="clothes hanger";
                        break;
                    case BUTTON:
                        type="button";
                        break;
                    case EGG:
                        type="egg";
                        break;
                    case STYROFOAM:
                        type="styrofoam";
                        break;
                    case TOILET_TISSUE:
                        type="toielt tissue";
                                break;
                }

                Intent i=new Intent(getApplicationContext(), MaterialListActivity.class);
                i.putExtra("condition", type);
                startActivity(i);

            }
        });
    }




    private class GridViewAdapter extends BaseAdapter
    {
        private List<GridItem> items=new ArrayList<>();
        private LayoutInflater inflater;

        public GridViewAdapter(Context context){
            inflater=LayoutInflater.from(context);
            items.add(new GridItem("종이류", R.drawable.paper));
            items.add(new GridItem("플라스틱", R.drawable.plastic));
            items.add(new GridItem("유리병", R.drawable.bottle));
            items.add(new GridItem("우유갑", R.drawable.milk_pack));
            items.add(new GridItem("병뚜껑", R.drawable.coca_cola));
            items.add(new GridItem("캔", R.drawable.can));
            items.add(new GridItem("아이스크림 막대", R.drawable.ice_cream_stick));
            items.add(new GridItem("옷걸이", R.drawable.clothes_hanger));
            items.add(new GridItem("단추", R.drawable.button));
            items.add(new GridItem("계란판", R.drawable.egg));
            items.add(new GridItem("스티로폼", R.drawable.styrofoam));
            items.add(new GridItem("휴지심", R.drawable.toilet_tissue));


        }

        @Override
        public int getCount() {return items.size();}

        @Override
        public Object getItem(int i){return items.get(i);}

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup)
        {
            View v=view;
            ImageView icon;
            TextView title;

            if(v==null)
            {
                v=inflater.inflate(R.layout.grid_item, viewGroup, false);
                v.setTag(R.id.gridIcon, v.findViewById(R.id.gridIcon));
                v.setTag(R.id.gridTitle, v.findViewById(R.id.gridTitle));
            }

            icon=(ImageView) v.getTag(R.id.gridIcon);
            title=(TextView) v.getTag(R.id.gridTitle);

            GridItem item=(GridItem) getItem(i);

            icon.setImageResource(item.getIconId());
            title.setText(item.getTitle());

            return v;
        }
    }
}
