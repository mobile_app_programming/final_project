package com.example.yeon.final_project;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

public class Third_tab extends AppCompatActivity {
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("즐겨찾기");
        setContentView(R.layout.activity_third_tab);

        loadDB();

    }

    public void loadDB(){

        db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );

        ListView listView = (ListView) findViewById(R.id.third_list);

        String sql = "SELECT * FROM people WHERE star = 1 ORDER BY best DESC;";

        Cursor c = db.rawQuery(sql, null);
        startManagingCursor(c);
        ListAdapter adapter = new ListAdapter(
                this,
                R.layout.listitem,
                c,
                new String[] {"name","reuse","best","menu"},
                new int[] {R.id.product_name, R.id.material, R.id.best_number,R.id.cat}
        );

        listView.setAdapter(adapter);

    }

    @Override
    protected void onPause() {
        super.onPause();

        db = openOrCreateDatabase("test.db", SQLiteDatabase.CREATE_IF_NECESSARY, null);

        if (db != null) {
            db.close();
        }
    }

    public void onSaveButtonClick(View v){
        if(db!=null){
            db.close();

            db = openOrCreateDatabase(
                    "test.db",
                    SQLiteDatabase.CREATE_IF_NECESSARY,
                    null
            );

            String sql = "SELECT * FROM people WHERE star = 1 ORDER BY best DESC;";
            Cursor c = db.rawQuery(sql, null);

            ListView listView = (ListView) findViewById(R.id.third_list);

            ListAdapter adapter = new ListAdapter(
                    this,
                    R.layout.listitem,
                    c,
                    new String[] {"name","menu","best"},
                    new int[] {R.id.product_name, R.id.material, R.id.best_number}
            );

            listView.setAdapter(adapter);
        }
    }
}
